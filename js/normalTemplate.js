function normalTemplate($container){

    var $hide = $container.find(".hide");
    var $key = $container.find(".key");
    var $name = $container.find(".name");
    var $service = $container.find(".service");
    var $type = $container.find(".type");
    var $subject = $container.find(".content-subject");
    var $content = $container.find(".content-body");
    var $result = $container.find(".result");

    var contentBody = scriptString($content.val(), false);
    var json = {
        hide: $hide.val(),
        key: $key.val(),
        name: $name.val(),
        service: $service.val(),
        type: $type.val(),
        content: {
            subject: $subject.val(),
            body: contentBody
        }
    };

    if (json.hide === "empty") {
        delete json.hide;
    } else {
        json.hide = (json.hide === 'true');
    }

    if (!json.service.length) {
        delete json.service;
    }

    if (!json.type.length) {
        delete json.type;
    }

    if (!json.content.subject.length) {
        delete json.content.subject;
    }

    $result.val(JSON.stringify(json, null, 2));
}