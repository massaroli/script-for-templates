$(document).ready(function () {

    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };

    $(".script-text-button").click( scriptText.bind(null, $(".text")) );
    $(".script-json-button").click( scriptJSON.bind(null, $(".text-json")) );
    $(".generate-whatsapp-template-button").click( whatsappTemplate.bind(null, $(".whatsapp")) );
    $(".generate-normal-template-button").click( normalTemplate.bind(null, $(".normal-template")) );
});