function scriptJSON($json){

    var str = $json.val()
      .replaceAll("\"\"", "\"")
      .replaceAll("\"{", "{")
      .replaceAll("},\"", "},");

    $json.val(str);
}