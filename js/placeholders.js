var PLACEHOLDERS = [
    {
        value: "${user.firstName}",
        toReplace: [
            "(nombre de vendedor)",
            "NOMBRE DE VENDEDOR",
            "--2--",
            "(nombre del vendedor)",
            "(NOMBRE DEL VENDEDOR)"
        ]
    },
    {
        value: "${user.lastName}",
        toReplace: [
            "(apellido de vendedor)",
            "APELLIDO DE VENDEDOR"
        ]
    },
    {
        value: "${prospect.firstName}",
        toReplace: [
            "(nombre de usuario)",
            "NOMBRE DE USUARIO",
            "--1--",
            "(nombre del cliente)",
            "(NOMBRE DEL CLIENTE)"
        ]
    },
    {
        value: "${prospect.lastName}",
        toReplace: [
            "(apellido de usuario)",
            "APELLIDO DE USUARIO"
        ]
    },
    {
        value: "${user.email}",
        toReplace: [
            "MAIL VENDEDOR",
        ]
    },
    {
        value: "${user.firstName} ${user.lastName}",
        toReplace: [
            "NOMBRE Y APELLIDO VENDEDOR",
        ]
    },
    {
        value: "${group.displayName}",
        toReplace: [
            "NOMBRE DE GRUPO",
            "--3--"
        ]
    }
];