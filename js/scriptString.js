/*
 * Reemplaza ciertos placeholder de un String y los reemplaza
 * @param {String} str
 * @param {boolean=} lineBreak Si es verdadero reemplaza los saltos de linea
 * por \n
 */
function scriptString(str, lineBreaks) {

    if (lineBreaks){
        str = str.replace(/(?:\r\n|\r|\n)/g, "\\n");
    }  

    // Remplaza
    PLACEHOLDERS.forEach(function(obj){
        obj.toReplace.forEach(function(s){
            str = str.replace(/\(|\)/, '')
                     .replace(/\{\{/, '--')
                     .replace(/\}\}/, '--')
                     .replaceAll(s, obj.value);
        });
    });

    return str;
}