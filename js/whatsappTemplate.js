function whatsappTemplate($wpp){
    var $key = $wpp.find(".key");
    var $name = $wpp.find(".name");
    var $type = $wpp.find(".type");
    var $content = $wpp.find(".content-body");
    var $result = $wpp.find(".result");
    var $language = $wpp.find(".language");

    function getWhatsappKeys(str) {
        var wppKeys = {
            "prospect.firstName": "prospectName",
            "user.firstName": "userName",
            "prospect.displayName": "groupName",
            "group.displayName": "groupName"
        }

        var keysToReturn = {}
        var keysFound = []
        for (var key in wppKeys) {
            if (str.search(key) !== -1) {
                keysFound.push(key);
            }
        }

        keysFound.sort(function(a, b) {
            return str.search(a) - str.search(b);
        });

        keysFound.forEach(function(k) {
            keysToReturn[wppKeys[k]] = k;
        });

        return keysToReturn;
    }

    function getParametersMap(whatsAppKeys){
        var arr = Object.keys(whatsAppKeys).map(function(key) {
            return key;
        });

        var parametersMap = {}
        for (var i = 0; i < arr.length; i++) {
            parametersMap[i] = arr[i]; 
        }

        return parametersMap;
    }

    var contentBody = scriptString($content.val(), false);
    var whatsAppKeys = getWhatsappKeys(contentBody);
    var parametersMap = getParametersMap(whatsAppKeys);
    var json = {
        key: $key.val(),
        name: $name.val(),
        type: $type.val(),
        service: "whatsAppApproved",
        locked: true,
        content: {
            subject: null,
            body: contentBody
        },
        whatsAppKeys: whatsAppKeys,
        templateConfig: {
            languageCode: $language.val(),
            parametersMap: parametersMap
        }
    };

    $result.val(JSON.stringify(json, null, 2));
}